/**
 *
 */
package jp.ne.zaq.rinku.bkbin005.sample.java.echo.echo;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Test;

/**
 * @author Mitsutoshi NAKANO
 *
 */
public class EchoTest extends Echo {

  /**
   * {@link jp.ne.zaq.rinku.bkbin005.sample.java.echo.echo.Echo#main(java.lang.String[])} のためのテスト・メソッド。
   */
  @Test
  public void testMain() {
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    System.setOut(new PrintStream(baos));
    final String[] args = { "a", "b" };
    final String expected = "a b\n";
    main(args);
    assertEquals(expected, baos.toString());
  }

}
