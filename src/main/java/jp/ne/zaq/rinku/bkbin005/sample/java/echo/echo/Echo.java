/**
 *
 */

package jp.ne.zaq.rinku.bkbin005.sample.java.echo.echo;

/**
 * Echo program.
 * @author Mitsutoshi NAKANO
 *
 */
public class Echo {
  /**
   * Main method.
   * @param args Command line arguments.
   */
  public static void main(String[] args) {
    for (int i = 0; i < args.length; ++i) {
      System.out.print(args[i]);
      System.out.print((i < args.length - 1) ? " " : "\n");
    }
  }

}
